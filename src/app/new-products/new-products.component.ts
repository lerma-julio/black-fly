import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-products',
  templateUrl: './new-products.component.html',
  styleUrls: ['./new-products.component.scss']
})
export class NewProductsComponent implements OnInit {

  products = [
    '../../assets/imgs/1.jpg',
    '../../assets/imgs/2.jpg',
    '../../assets/imgs/3.jpg',
    '../../assets/imgs/4.jpg',
    '../../assets/imgs/5.jpg',
    '../../assets/imgs/6.jpg',
    '../../assets/imgs/7.jpg',
    '../../assets/imgs/9.jpg',
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
