import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  carouselInfo = [
    '/assets/imgs/slide1.jpg',
    '/assets/imgs/slide2.jpg',
    '/assets/imgs/slide3.jpg'
  ]

  selectedSlideIndex = 0;

  constructor() { }

  ngOnInit(): void {
  }

  onRightClick(){
    if(this.selectedSlideIndex + 1 === this.carouselInfo.length){
      this.selectedSlideIndex = 0;
      return;
    }
    this.selectedSlideIndex++;
  }

  onLeftClick(){
    if(this.selectedSlideIndex === 0){
      this.selectedSlideIndex = this.carouselInfo.length - 1;
      return;
    }
    this.selectedSlideIndex--;
  }

}
